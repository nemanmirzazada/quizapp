package com.neman.quizapp.Repository;

import com.neman.quizapp.Model.Question;
import com.neman.quizapp.Model.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizRepository extends JpaRepository<Quiz,Long> {

}
