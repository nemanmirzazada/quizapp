package com.neman.quizapp.Service;

import com.neman.quizapp.Model.QuestionWrapper;
import com.neman.quizapp.Model.Response;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface QuizService {

    ResponseEntity<String> createQuiz(String category, int numQ, String title);

    ResponseEntity<List<QuestionWrapper>> getQuizQuestions(long id);

    ResponseEntity<Integer> calculateResult(long id, List<Response> responses);
}
