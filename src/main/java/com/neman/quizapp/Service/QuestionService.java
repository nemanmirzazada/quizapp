package com.neman.quizapp.Service;

import com.neman.quizapp.Model.Question;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface QuestionService {

    ResponseEntity<List<Question>> getAllQuestions();

    ResponseEntity<List<Question>> getQuestionByCategory(String category);

    ResponseEntity<String> addQuestion(Question question);


}
