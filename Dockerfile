FROM openjdk
RUN echo "quiz-app"
COPY ./build/libs/quizapp-0.1.jar /app/
WORKDIR /app/


ENTRYPOINT ["java"]
CMD ["-jar", "/app/quizapp-0.1.jar"]
